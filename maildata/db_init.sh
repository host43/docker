#!/bin/bash

echo $Domain
echo $MasterEmail
echo $MasterPassword

#mysql -uroot -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE -e \
#"CREATE TABLE IF NOT EXISTS mail_domain (
#  id INT NOT NULL AUTO_INCREMENT,
#  name VARCHAR(50) NOT NULL,
#  PRIMARY KEY (id),
#  UNIQUE KEY name (name)
#) ENGINE=InnoDB DEFAULT CHARSET=utf8;
#
#CREATE TABLE IF NOT EXISTS mail_user (
#  id INT NOT NULL AUTO_INCREMENT,
#  domain_id INT NOT NULL,
#  email VARCHAR(100) NOT NULL,
#  password VARCHAR(106) NOT NULL,
#  descr VARCHAR (100),
#  PRIMARY KEY (id),
#  UNIQUE KEY email (email),
#  FOREIGN KEY (domain_id) REFERENCES mail_domain(id) ON DELETE CASCADE
#) ENGINE=InnoDB DEFAULT CHARSET=utf8;
#
#CREATE TABLE IF NOT EXISTS mail_alias (
#  id INT NOT NULL AUTO_INCREMENT,
#  domain_id INT NOT NULL,
#  src VARCHAR (100) NOT NULL,
#  dst LONGTEXT NOT NULL,
#  descr VARCHAR (100),
#  PRIMARY KEY (id),
#  FOREIGN KEY (domain_id) REFERENCES mail_domain (id) ON DELETE CASCADE
#) ENGINE=InnoDB DEFAULT CHARSET=utf8;
#
#INSERT INTO mail_domain(name) VALUES ('$Domain');
#INSERT INTO mail_user(domain_id,email,password,descr)
#  SELECT id,
#    '$MasterEmail',
#    '$MasterPassword',
#    'MASTER EMAIL'
#  FROM mail_domain WHERE name='$Domain';
#"
#INSERT INTO mail_user(domain_id,email,password,descr)
#  SELECT id,
#    '$MasterEmail',
#    MD5('$MasterPassword'),
#    'MASTER EMAIL'
#  FROM mail_domain WHERE name='$Domain';
#"

mysqladmin create $ROUNDCUBE_DB -uroot -p$MYSQL_ROOT_PASSWORD

mysql -uroot -p$MYSQL_ROOT_PASSWORD $ROUNDCUBE_DB -e \
"GRANT ALL PRIVILEGES ON rc.* TO 'rcadmin'@'%' identified by 'rcadmin';
"
#postfixadmin
#mysqladmin create postfixadmin -uroot -p$MYSQL_ROOT_PASSWORD
