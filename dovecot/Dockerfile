FROM ubuntu:latest
MAINTAINER Stanislav Khokhlov <xost43@gmail.com>

ARG Hostname
ARG DomainName
ARG DbDriver
ARG DbUser
ARG DbPassword
ARG DbHost
ARG DbName
ARG TZ

RUN echo ${Hostname} > /etc/hostname
RUN echo "127.0.0.1 localhost localhost.localdomain mail ${Hostname}.${DomainName}" > /etc/hosts \
  && chown root:root /etc/hosts

RUN ln -snf /usr/share/zoneinfo/${TZ} /etc/localtime
RUN echo ${TZ} > /etc/timezone

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive \
apt-get install -qq -y \
apt-utils \
dovecot-common \
dovecot-mysql \
dovecot-imapd \
dovecot-pop3d \
dovecot-lmtpd \
rsyslog \
ntp \
tzdata \
vim \
mysql-client \
net-tools

RUN echo "driver = mysql \n\
connect = host=${DbHost} dbname=${DbName} user=${DbUser} password=${DbPassword} \n\
default_pass_scheme = CRAM-MD5 \n\
password_query = SELECT username AS user, password FROM mailbox WHERE username='%u' AND active='1'" > /etc/dovecot/dovecot-sql.conf
ADD dovecot.conf /etc/dovecot/dovecot.conf

RUN groupadd -g 5000 vmail
RUN useradd -m -s /bin/false -u 5000 -g vmail vmail
RUN groupadd postfix -g 500
RUN useradd -u 500 -g 500 postfix

EXPOSE 110 143 993 995

CMD ["sh","-c","service rsyslog start ; service dovecot start ; tail -f /dev/null"]
